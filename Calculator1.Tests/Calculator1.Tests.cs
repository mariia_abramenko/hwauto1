﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalcLib;

namespace Calculator1.Tests

{
    [TestFixture]
    public class CalculatorUnitTest
    {
       // CalculatorUnitTest MyCalcLib = new CalculatorUnitTest();
        [Test]
        public void TestAdd()
        {
           Assert.AreEqual(15, MyCalc.Add(5,10),"should be equal 15");
        }

        [Test]
        public void TestSubs()
            {
            
            Assert.AreEqual(5,MyCalc.Subs(10,5), "10-5 should be equal to 5");
        }

        [Test]
        public void TestDivide()
        {

            Assert.AreEqual(2, MyCalc.Divide(10,5),"10/5 should be equal to 2");
        }

        [Test]
        public void TestMultiply()
        {

            Assert.AreEqual(20,MyCalc.Multiply(2,10),"2*10 should be equal to 20");
        }


        [Test]
        public void TestPow()
        {
            Assert.AreEqual(32, MyCalc.Pow(2,5), "2^5 should be equal to 32");
        }


        [Test]
        public void TestPercent()
        {

            Assert.AreEqual(18,MyCalc.Percent(180, 10), "10% from 180should be equal to 10");
        }

        [Test]
        public void TestArrSum()
        {
            
            Assert.AreEqual(15, MyCalc.ArrSum(15), "sum of the {0,1,2,3,4,5} array should be equal 15");
        }
        [Test]
        public void TestMinValArr()
        {
            Assert.AreEqual(-2,MyCalc.MinVal(-2), "Minimum value of the of the { 9, 3, 5, 1, 10, -2 } array should be equal -2");
        }

        [Test]
        public void TestMaxValArr()
        {
            Assert.AreEqual(10, MyCalc.MaxVal(10), "Maximum value of the of the { 9, 3, 5, 1, 10, -2 } array should be equal 10");
        }

        [Test]
        public void TestMultiply1()
        {
            Assert.Zero(MyCalc.Multiply(1,0),"1*0 should be equal 0");
        }

        [Test]
        public void TestAdd2()
        {
            Assert.IsTrue(1==MyCalc.Add(0,1),"0+1 should be equal 1");
        }

        [Test]
        public void TestPercent1()
        {
            Assert.True(10 == MyCalc.Percent(10, 100), "10% from 100 is equal 10");
        }

        [Test]
        public void TestPercent2()
        {
            Assert.Less(9, MyCalc.Percent(10, 100), "10% from 100 is equal 10");
        }

        [Test]
        public void TestSubs1()
        {
            Assert.NotZero(MyCalc.Subs(4, 4), "4-4 should be equal to 0");
        }

        [Test]
        public void TestPow1()
        {
            Assert.IsFalse(32 == MyCalc.Pow(2, 5), "2^5 should be equal 32");
        }

        [Test]
        public void TestDivide1()
        {
            Assert.Greater(4, MyCalc.Divide(50, 10), "50/10 should be equal 5");
        }

    }
}
