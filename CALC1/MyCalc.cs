﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyCalcLib
{
    public class MyCalc
    {
        
            public static int Add(int a, int b)
            {

                return a + b;
            }

            public static int Subs(int a, int b)
            {

                return a - b;
            }

            public static int Divide(int a, int b)
            {

                return a / b;
            }

            public static int Multiply(int a, int b)
            {

                return a * b;
            }

            public static int Pow(int a, int n)
            {
                int k = 0;
                int b = 1;

                while (k != n)
                {
                    k++;
                    b *= a;
                }
                return b;

            }

            public static int Percent(int a, int b)
            {
                return a * b / 100;
            }

            public static int ArrSum(int sum)
            {
                int[] numbers = new int[] { 0, 1, 2, 3, 4, 5 };
                sum = 0;
                foreach (int value in numbers)
                {
                    sum += value;
                }

                return sum;
            }

            public static int MinVal(int minElement)
            {
                int[] numbers = { 9, 3, 5, 1, 10, -2 };
                minElement = numbers[0];
                foreach (int element in numbers)
                {
                    if (element < minElement)
                    {
                        minElement = element;
                    }
                }

                return minElement;
            }

            public static int MaxVal(int maxElement)
            {
                int[] numbers = { 9, 3, 5, 1, 10, -2 };
                maxElement = numbers[0];
                foreach (int element in numbers)
                {
                    if (maxElement < element)
                    {
                        maxElement = element;
                    }
                }

                return maxElement;
            }

        static void Main(string[] args)
        {
            //i dont actually understand how program supposed to work without the main method
        }
    }
}
